---
name:  "Compte rendu entretien Andy Floch"
date:   2020-12-10 10:18:32 +0200
students: Manuel
online:
repo: https://gitlab.com/ManuelPerennes/compte-rendu-andy-floch-md
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
style: blood
---

## Questionnaire d’enquête métier

///

#### ❖ Quel est ton parcours professionnel : formation ? diplôme ? expériences pro ? 
Mes deux missions principales étaient chez Orange et Soprasteria en tant que testeur, c'était déja il a quelques années. J'ai également travaillé chez Psikio (entrepise de portage en bretagne) pour de la création de site.
Actuellement je suis chez Celad envoyé sur deux missions. 

///

#### Depuis combien de temps tu es dans cette boîte ? Cela fait deux ans. Pourquoi le développement (choix du métier) ? 
J'ai toujours adoré l’informatique. Plus particulièrement l'IA. Malheureusement à l'époque le secteur était bouché je suis donc allé sur du dev'.

///

#### ❖ Peux-tu me décrire ton métier ? Quels projets as-tu effectué ? Quels langages tu utilises ?
Je suis dev obs'. Je developpe du script et m"occupe principalement de la  CI/CDE sur git lab. C'est de l'intégration d’application. 
Bcp de monitoring. Outil qui permet de check la santé des appli chez les clients. 
Je travail principalement sur le langage python bash (langage de la commande).

///

#### ❖ Quels  sont ton cadre et tes conditions de travail : seul ? en équipe ? freelance ?quels collègues ? 
Seul en tant que dev obs mais je travaille et me coordone avec toutes les equipes.

///

#### ❖ Peux-tu m’indiquer ta rémunération ? En début de carrière ? En ce moment ? 
J'ai été au forfait un temps car indépendant. J'ai été jusqu'à 2500 en portage salarial et chez celad env 2000. (net)? Je suis très sollicité pour des postes avec de meilleures remunérations? Mais je reste à Celad pour des raisons persos.

///

#### ❖ Comment tu te sens dans ton travail ? 
Très bien chez Celad. Mais c'est compliqué chez le client.Le management des developpeur est discutable mais super equipe technique. 

///

#### ❖ Comment tu te nourris ? Est-ce que tu fais des interventions ? Comment tu nourris ton réseau ?
Linkedin principalement. Faire un profil le plus complet possible. Ajouter un maximum de connection. 

///

#### ❖ Dans tes projets, comment tu effectues la veille ? Qu’est-ce qui t’intéresse en ce moment ? 
J'ai un interêt particulier pour le langage Go et les solutions cloud open source comme le stack et la cybersecurité.

///

#### ❖ Utilises-tu des outils de veille automatique ? 
Non. Je n'en ai pas l'utilité.

///

#### ❖ Quelles sont tes perspectives d’évolution ? dans 5 ans ? 10 ans ? 
Dans 5 ans eventuellement monter mon entreprise de cyber secu. Me tourner le plus possible vers de l’indépendance et si possible l’IA recherche. J'ai pleins d'idées et il y a de nombreuses possibilités.

 ///

#### ❖ Quels problèmes tu rencontres dans ton métier ? Quels sont les inconvénients de ce métier ?
Selon les entreprises, les process et la gestion managerial peuvent être questionnables. Les idées ne sont pas toujours écoutées. La différence de point de vue entre la partie opérationnel et la partie technique peuvent être frustrantes.

///

#### ❖ Es-tu ou as-tu été confronté au syndrome de l’imposteur ? 
Non. Je n'ai jamais eu ce sentiment et ne connait pas de dev qui ont ce syndrome. 

///

#### ❖ Qu’est-ce qui fait un bon développeur ? 
La méthode, la capacité a etre organisé. La perseverance. La hierarchie, il faut etre capable de travailler  et coder correctement meme si la hierarchie impose des delais intenable. Etre capable de bien chiffrer les taches. (temps budget )

///

#### ❖ Qu’est-ce que vous cherchez comme profil dans ta boîte ? Quelles qualités sont essentielles pour un développeur ? 
Organistation méthode rigueur et propreté du code. Les profils recherchés sont multiples. 

///

#### ❖ Quels sont les pics d‘activité dans ton secteur ? Y’a-t-il des périodes plus propices pour postuler ? 
(Il a un profil solides donc est très demandé.)
Je trouve le calendrier est biaisé au vue de la période. Sinon probablement le début d’année (budget), les entreprises préparent leur budget à ce moment la. Période de l’été également en préparation de septembre.

///

#### ❖ Avez-vous pris des stagiaires dans le passé ? Y’a-t-il des possibilités d’effectuer un stage dans ton entreprise ? 
Oui j'ai régulièrement travaillé et formé des stagiaires. Je prendrai probablement Manu si l'entreprise le permet. 

///

#### ❖ Est-ce que tu accepterais éventuellement de venir témoigner dans la promo ? 
Période compliquée mais oui. 